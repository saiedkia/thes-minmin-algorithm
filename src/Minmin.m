function [makespan,flowtime,utilization,map]=Minmin(etc)
u=[];
w=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
flowtime=0;
for i=1:512  %Unmapped Task
    u=union(u,i);
end
while(size(u,2)>0) % Unmapped Task exist 
for i=1:size(u,2)  %each unmapped task
    for j=1:16   %each machine
        c(u(i),j)=w(j)+etc(u(i),j);  %cp time task for each machine
    end
    [x,m]=min(c(u(i),:));  % Task U(i) Machine m?
    z(u(i))=x;      %min value for task U(i)
    machine(u(i))=m; %machine index for task U(i)
 end
[x,i]=min(z(1,:));  % i is number of task with min overall cp time
flowtime=flowtime+etc(i,machine(i));
map(i)=machine(i);
w(machine(i))=w(machine(i))+etc(i,machine(i)); %Update Machine Workload
u=setdiff(u,i);   % Detele from unmapped task set
z(i)=nan; %Not a number(task i mapped)
end %End while
makespan=max(w);
utilization=sum(w)/(makespan*16);
end