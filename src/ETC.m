% ETC Matrix Range Based Method
%Expected Time to Compute
function result=ETC(t,m)  %Inconsistent Matrix
for i=1:512
    x=randi(t);
    for j=1:16
        result(i,j)=x*randi(m);
    end
end
end