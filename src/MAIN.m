clear;
clc;
close all
sumit=zeros(12,2);
for itera=1:1
    disp([ num2str(itera) ' iteration is begin']);
    % Compatible-LowLow
    etc=ETC(10,10);
    % save etc;
    % etc=sort(etc,2);
    % etc=Partially(etc);
    % olbm=Olb(etc)
    % metm=Met(etc)
    % mctm=Mct(etc)
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(1,1)=sumit(1,1)+minminm;
    sumit(1,2)=sumit(1,2)+Fval;
    % Compatible-LowHigh
    etc=ETC(10,100);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(2,1)=sumit(2,1)+minminm;
    sumit(2,2)=sumit(2,2)+Fval;
    % Compatible-HighLow
    etc=ETC(100000,10);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(3,1)=sumit(3,1)+minminm;
    sumit(3,2)=sumit(3,2)+Fval;
    % Compatible-HighHigh
    etc=ETC(100000,100);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(4,1)=sumit(4,1)+minminm;
    sumit(4,2)=sumit(4,2)+Fval;
    % Incompatible-LowLow
    etc=ETC(10,10);
    etc=sort(etc,2);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(5,1)=sumit(5,1)+minminm;
    sumit(5,2)=sumit(5,2)+Fval;
    % Incompatible-LowHigh
    etc=ETC(10,100);
    etc=sort(etc,2);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(6,1)=sumit(6,1)+minminm;
    sumit(6,2)=sumit(6,2)+Fval;
    % Incompatible-HighLow
    etc=ETC(100000,10);
    etc=sort(etc,2);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(7,1)=sumit(7,1)+minminm;
    sumit(7,2)=sumit(7,2)+Fval;
    % Incompatible-HighHigh
    etc=ETC(100000,100);
    etc=sort(etc,2);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(8,1)=sumit(8,1)+minminm;
    sumit(8,2)=sumit(8,2)+Fval;
    % Partially-LowLow
    etc=ETC(10,10);
    etc=Partially(etc);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(9,1)=sumit(9,1)+minminm;
    sumit(9,2)=sumit(9,2)+Fval;
    % Partially-LowHigh
    etc=ETC(10,100);
    etc=Partially(etc);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(10,1)=sumit(10,1)+minminm;
    sumit(10,2)=sumit(10,2)+Fval;
    % Partially-HighLow
    etc=ETC(100000,10);
    etc=Partially(etc);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(11,1)=sumit(11,1)+minminm;
    sumit(11,2)=sumit(11,2)+Fval;
    % Partially-HighHigh
    etc=ETC(100000,100);
    etc=Partially(etc);
    minminm=Minmin(etc);
    Fval=gapso(60,10,etc);
    sumit(12,1)=sumit(12,1)+minminm;
    sumit(12,2)=sumit(12,2)+Fval;
end

A=cell(14,3);
A{1,1}='Compare GA-PSO vs MINMIN';
A{2,3}='GA-PSO';
A{2,2}='MINMIN';
A{3,1}='Compatible-LowLow';
A{4,1}='Compatible-LowHigh';
A{5,1}='Compatible-HighLow';
A{6,1}='Compatible-HighHigh';
A{7,1}='Incompatible-LowLow';
A{8,1}='Inompatible-LowHigh';
A{9,1}='Incompatible-HighLow';
A{10,1}='Incompatible-HighHigh';
A{11,1}='Partially-LowLow';
A{12,1}='Partially-LowHigh';
A{13,1}='Partially-HighLow';
A{14,1}='Partially-HighHigh';
for ii=1:12
    A{ii+2,2}=sumit(ii,1)/20;
    A{ii+2,3}=sumit(ii,2)/20;
end


filename = 'CompareGaPSOvsMinMin.xlsx';
sheet = 1;
xlRange ='A1';
xlswrite(filename,A,sheet,xlRange)

beep